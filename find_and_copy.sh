#! /usr/bin/env zsh


######################
#
# First create the folder where the album should be.
# Run the script with ./find_and_copy.sh 'search_directory' 'album_directory'
# Depends on youtube-dl for downloading songs from youtube.
#
######################

IFS=$'\n'       # make newlines the only separator
set -f
cd "$(dirname "$0")"

search_function() {
    find "./" -type f -iname "*$1*"
}

youtube_dl_function() {
    yt_url=$1
    download_dir=$2
    song_name=$3
    song_number=$4
    while ! [ $yt_url ]; do
        echo 'Enter a valid youtube url.'
        read yt_url
    done
    song_location=$(youtube-dl -x $1 --audio-format=wav --audio-quality=0 | sed -n -e '/\[ffmpeg\]/ s/.*\: *//p')
    new_location="$download_dir/$song_number - $song_name.wav"
    mv $song_location $new_location
    echo "Copied to: $new_location\n\n"
    echo "Song number $song_number: $song_name" | tee -a "$download_dir/copied_songs.txt"
}

find_songs() {
    song_number=1
    for song_name in $(cat "$1"); do
        echo -e "Searching for \"$song_name.\""
        found_songs=$(search_function "$song_name")
        selected_song_number=0
        while [ $selected_song_number -eq 0 ]; do
            while ! [ $found_songs ]; do
                echo 'Nothing found, specify a customized search frase (enter -1 to retry search or "!yt" to search and download from youtube.):'
                read custom_search_frase
                if [[ $custom_search_frase == '!yt' ]]; then
                    echo 'Enter youtube url to download from:'
                    read yt_url
                    youtube_dl_function "$yt_url" "$2" "$song_name" "$song_number"
                    break
                elif [[ $custom_search_frase == '-1' ]]; then
                    found_songs=$(search_function "$song_name")
                else
                    found_songs=$(search_function "$custom_search_frase")
                fi
            done
            counter=1
            for version in $(echo "$found_songs"); do
                echo -e "$counter: $version"
                counter=$(($counter + 1))
            done
            echo 'Choose a song to copy (enter 0 to search for a custom frase or -1 to retry or "!yt" to download from youtube):'
            read selected_song_number
            if [[ $selected_song_number == '!yt' ]]; then
                echo 'Enter youtube url to download from:'
                read yt_url
                youtube_dl_function "$yt_url" "$2" "$song_name" "$song_number"
                break
            elif [ $selected_song_number -eq '-1' ]; then
                echo 'Retrying'
            elif [ $selected_song_number -ne 0 ]; then
                selected_song=$(echo $found_songs | sed -n "${selected_song_number}p")
                echo "Song number $song_number: $selected_song" | tee -a "$2/copied_songs.txt"
                if [[ $(echo $selected_song | sed 's/^.*\(.\{3\}\)/\1/') == 'mp3' ]]; then
                    song_format='mp3'
                elif [[ $(echo $selected_song | sed 's/^.*\(.\{3\}\)/\1/') == 'wav' ]]; then
                    song_format='wav'
                else
                    echo 'Invalid format. Skipping song.'
                    break
                fi
                cp "$selected_song" "$2$song_number - $song_name.$song_format"
                echo "Copied to: $2$song_number - $song_name.$song_format\n\n"
                song_number=$(($song_number + 1))
            else
                unset -v found_songs
            fi
        done
    done
}

if [ "$1" ]; then
    if [ "$2" ]; then
        touch "$2/copied_songs.txt"
        echo $(date) > "$2/copied_songs.txt"
        find_songs "$1" "$2"
    else
        echo 'Specify path where songs should be copied.'
    fi
else
    echo 'Specify path to file with song names.'
fi
